package LQ3550_messy_arrays;

import java.util.*;

//C(l,2)=l*(l-1)/2=n
/*
ll-l-2n=0
l=(1+sqrt(1+8n))/2
 */
public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int l = (int) (1 + Math.sqrt(1 + 8 * n)) / 2;
		while (l > 0) {
			System.out.print(l + " ");
			l--;
		}
		scanner.close();
	}
}
