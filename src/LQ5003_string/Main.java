package LQ5003_string;

import java.util.*;

// 2023 Manacher
public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = scanner.next();
		int n = str.length();
		int c = 0;
		int c0 = 0;
		int[] d1 = new int[n];
		int[] d2 = new int[n];
		int[] c1 = new int[n];
		int[] c2 = new int[n];
		int l = -1, r = -1;
		for (int i = 0; i < n; i++) {
			int s1 = 0, s2 = 0, t1 = 0, t2 = 0;
			if (i < r) {
				if (d1[l + r - i] <= r - i) {
					s1 = d1[l + r - i];
					t1 = c1[l + r - i];
				}
				if (d2[l + r - i - 1] <= r - i) {
					s2 = d2[l + r - i - 1];
					t2 = c2[l + r - i - 1];
				}
				System.out.print("");
			}
			int j = s1, k = t1;
			if (str.charAt(i) == '0') {
				while (i - j - 1 >= 0 && i + j + 1 < n) {
					if (str.charAt(i - j - 1) == str.charAt(i + j + 1)) {
						if (str.charAt(i - j - 1) == '1') {
							k += 1;
						}
						j += 1;
					} else {
						break;
					}
				}
				c1[i] = k;
				d1[i] = j;
				c = Math.max(c, k);
				if (i + j > r) {
					System.out.print("");
					l = i - j;
					r = i + j;
					System.out.print("");
				}
			} else {
				c0 += 1;
			}
			j = s2;
			k = t2;
			while (i - j >= 0 && i + j + 1 < n) {
				if (str.charAt(i - j) == str.charAt(i + j + 1)) {
					if (str.charAt(i - j) == '1') {
						k += 1;
					}
					j += 1;
				} else {
					break;
				}
			}
			c2[i] = k;
			d2[i] = j;
			c = Math.max(c, k);
			if (j > 0 && i + j > r) {
				System.out.print("");
				l = i - j + 1;
				r = i + j;
				System.out.print("");
			}
		}
		System.out.println(c0 - c);
		scanner.close();
	}
}
