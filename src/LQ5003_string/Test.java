package LQ5003_string;

import java.util.*;

//2023 Manacher
public class Test {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = scanner.next();
		int n = str.length();
		int c = 0;
		int c0 = 0;
		for (int i = 0; i < n; i++) {
			int j = 0, k = 0;
			if (str.charAt(i) == '0') {
				while (i - j - 1 >= 0 && i + j + 1 < n) {
					if (str.charAt(i - j - 1) == str.charAt(i + j + 1)) {
						if (str.charAt(i - j - 1) == '1') {
							k += 1;
						}
						j += 1;
					} else {
						break;
					}
				}
				c = Math.max(c, k);
			} else {
				c0 += 1;
			}
			j = k = 0;
			while (i - j >= 0 && i + j + 1 < n) {
				if (str.charAt(i - j) == str.charAt(i + j + 1)) {
					if (str.charAt(i - j) == '1') {
						k += 1;
					}

					j += 1;
				} else {
					break;
				}
			}
			c = Math.max(c, k);
		}
		System.out.println(c0 - c);
		scanner.close();
	}
}