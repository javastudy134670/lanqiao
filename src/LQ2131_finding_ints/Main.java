package LQ2131_finding_ints;

import java.util.*;

// CTR exgcd
public class Main {
	static List<Integer> primeList = primeEuler(49);

	public static void main(String[] args) {
		int[] r = { 0, 0, 1, 2, 1, 4, 5, 4, 1, 2, 9, 0, 5, 10, 11, 14, 9, 0, 11, 18, 9, 11, 11, 15, 17, 9, 23, 20, 25,
				16, 29, 27, 25, 11, 17, 4, 29, 22, 37, 26, 9, 1, 11, 11, 33, 29, 15, 5, 41, 46 };
		long M = 1, ans = 0;
		HashSet<Integer> set = new HashSet<>();
		for (Integer prime : primeList) {
			if (r[prime] != 0) {
				M *= prime;
				set.add(prime);
			}
		}
		for (Integer e : set) {
			long M0 = M / e;
			ans += r[e] * M0 * new Exgcd(M0, e).x;
			ans %= M;
		}
		System.out.println(ans);
		return;
	}

	static List<Integer> primeEuler(int n) {
		List<Integer> prime_list = new ArrayList<>();
		HashSet<Integer> primeCheck = new HashSet<>();
		for (int i = 2; i <= n; i++) {
			if (!primeCheck.contains(i)) {
				prime_list.add(i);
			}
			for (Integer prime : prime_list) {
				primeCheck.add(prime * i);
				if (prime % i == 0) {
					break;
				}
			}
		}
		return prime_list;
	}

	// ans*a = 1 (% n)
	// ans = a^(n-2)
	static int exgcdSpecial(int a, int n) {
		a %= n;
		int ans = 1;
		for (int i = 0; i < n - 2; i++) {
			ans *= a;
			ans %= n;
		}
		return ans;
	}
}

class Exgcd {
	long x;
	long y;

	// ax+yn = gcd(a,b)
	Exgcd(long a, long n) {
		exgcd(a, n);
	}

	void exgcd(long a, long n) {
		if (n == 0) {
			x = 1;
			y = 0;
		} else {
			exgcd(n, a % n);
			long tmp = x;
			x = y;
			y = tmp - a / n * y;
			System.out.print("");
		}
	}

	@Override
	public String toString() {
		return "x=" + x + ",y=" + y;
	}
}
