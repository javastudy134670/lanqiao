package LQ1151_min_sum_of_prime_factors;

import java.util.*;

public class Main {
	public static void main(String[] args) {
		HashMap<Integer, Integer> idx = new HashMap<>();
		HashSet<Integer> check = new HashSet<>();
		List<Integer> prime = new ArrayList<>();
		List<Integer> input = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 0; i < n; i++) {
			int tmp = scanner.nextInt();
			idx.put(tmp, i);
			input.add(tmp);
		}
		input.sort(Comparator.naturalOrder());
		for (int i = 2; i <= input.get(n - 1); i++) {
			if (check.contains(i)) {
				check.remove(i);
			} else {
				prime.add(i);

			}
			for (Integer p : prime) {
				check.add(p * i);
				if (p % i == 0) {
					break;
				}
			}
		}
		int sum = 0;
		int index = 0;
		for(int i=2;i<=n;i++) {
			for(Integer p:prime) {
				if(n%p==0) {
					sum+=p;
					break;
				}
			}
			if(n==input.get(index)) {
				
			}
		}
		scanner.close();
	}
}