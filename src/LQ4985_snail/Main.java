package LQ4985_snail;

import java.util.*;

//2023 snail dp
public class Main {
	static int n;
	static int[] x;
	static int[] start;
	static int[] end;
	static HashMap<Integer, Double> crawl = new HashMap<>();
	static HashMap<Integer, Double> fly = new HashMap<>();

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		n = scanner.nextInt();
		x = new int[n];
		start = new int[n];
		end = new int[n - 1];
		for (int i = 0; i < n; i++) {
			x[i] = scanner.nextInt();
		}
		for (int i = 0; i < n - 1; i++) {
			start[i] = scanner.nextInt();
			end[i] = scanner.nextInt();
		}
		start[n - 1] = 0;
		scanner.close();
		System.out.printf("%.2f%n", x[0] + Math.min(crawlDp(0), start[0] / 0.7 + flyDp(0)));
		return;
	}

	static double crawlDp(int idx) {
		if (idx == n - 1) {
			return 0;
		}
		if (!crawl.containsKey(idx)) {
			crawl.put(idx, x[idx + 1] - x[idx] + Math.min(crawlDp(idx + 1), start[idx + 1] / 0.7 + flyDp(idx + 1)));
		}
		return crawl.get(idx);
	}

	static double flyDp(int idx) {
		if (idx == n - 1) {
			return 0;
		}
		if (!fly.containsKey(idx)) {
			int dif = end[idx] - start[idx + 1];
			double cost = (dif > 0) ? dif / 1.3 : -dif / 0.7;
			fly.put(idx, Math.min(end[idx] / 1.3 + crawlDp(idx + 1), cost + flyDp(idx + 1)));
		}
		return fly.get(idx);
	}

}
