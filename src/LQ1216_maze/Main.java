package LQ1216_maze;

import java.util.*;

public class Main {
	static HashSet<Position> maze = new HashSet<Position>();

	public static void main(String[] args) {
		LinkedList<Position> queue = new LinkedList<>();
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt(), m = scanner.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				maze.add(new Position(i, j));
			}
		}
		Position start = new Position(scanner.nextInt(),scanner.nextInt()), end=new Position(scanner.nextInt(),scanner.nextInt());
		queue.add(start);
		maze.remove(end);
		Position p;
		while(!queue.isEmpty()) {
			p = queue.poll();
			if(p.equals(end)) {
				break;
			}
		}
		scanner.close();
	}
}

class Position {
	int x;
	int y;

	Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return hashCode() == obj.hashCode();
	}
}