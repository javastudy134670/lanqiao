package LQ4124_divide_candies;

import java.util.*;

//dp
public class Main {
	public static void main(String[] args) {
		System.out.println(dp_f(9, 16, 0));
		return;
	}

	static HashMap<Triple, Integer> dp = new HashMap<>();
	static {
		dp.put(new Triple(0, 0, 7), 1);
	}

	static int dp_f(int A, int B, int k) {
		Triple key = new Triple(A, B, k);
		if (!dp.containsKey(key)) {
			int sum = 0;
			for (int i = 0; i <= A; i++) {
				for (int j = 0; j <= B; j++) {
					if (i + j >= 2 && i + j <= 5) {
						sum += dp_f(A - i, B - j, k + 1);
					}
				}
			}
			dp.put(key, sum);
		}
		return dp.get(key);
	}
}

class Triple {
	int x, y, z;

	Triple(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z);
	}

	@Override
	public boolean equals(Object obj) {
		return hashCode() == obj.hashCode();
	}
}
