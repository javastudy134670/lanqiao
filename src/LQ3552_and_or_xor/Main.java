package LQ3552_and_or_xor;

public class Main {
	public static void main(String[] args) {
		int num = (int) Math.pow(3, 10);
		int ans = 0;
		for (int i = 0; i < num; i++) {
			int tmp = i;
			int[] arr = { 1, 0, 1, 0, 1 };
			for (int x = 0; x < 4; x++) {
				for (int y = 0; y < 4 - x; y++) {
					int k = tmp % 3;
					tmp /= 3;
					switch (k) {
					case 0:
						arr[y] = arr[y] & arr[y + 1];
						break;
					case 1:
						arr[y] = arr[y] | arr[y + 1];
						break;
					default:
						arr[y] = arr[y] ^ arr[y + 1];
					}
				}
			}
			if (arr[0] == 1) {
				ans += 1;
			}
		}
		System.out.println(ans);
	}
}
