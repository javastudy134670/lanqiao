package LQ1265_sort;

import java.util.*;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		// 在此输入您的代码...
		int n = scan.nextInt();
		PriorityQueue<Integer> queue = new PriorityQueue<>();
		Stack<Integer> stack = new Stack<>();
		for (int i = 0; i < n; i++) {
			queue.add(scan.nextInt());
		}
		for (int i = 0; i < n; i++) {
			stack.add(queue.poll());
			System.out.print(stack.peek() + " ");
		}
		System.out.println();
		for (int i = 0; i < n; i++) {
			System.out.print(stack.pop() + " ");
		}
		scan.close();
	}
}