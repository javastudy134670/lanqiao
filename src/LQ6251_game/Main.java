package LQ6251_game;

import java.util.*;

//2023 游戏
public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt(), k = scanner.nextInt();
		int i = 0;
		double P_sum = 0;
		double Q_sum = 0;
		LinkedList<Integer> P_QUE = new LinkedList<>(), Q_QUE = new LinkedList<>(), data = new LinkedList<>();
		for (int j = 0; j < k; j++) {
			int num = scanner.nextInt();
			while (!P_QUE.isEmpty() && P_QUE.getLast() < num) {
				P_QUE.removeLast();
			}
			while (!Q_QUE.isEmpty() && Q_QUE.getLast() > num) {
				Q_QUE.removeLast();
			}
			P_QUE.add(num);
			Q_QUE.add(num);
			data.add(num);
			i += 1;
		}
		P_sum += P_QUE.getFirst();
		Q_sum += Q_QUE.getFirst();
		while (i < n) {
			int first = data.remove();
			if (first == P_QUE.getFirst()) {
				P_QUE.remove();
			}
			if (first == Q_QUE.getFirst()) {
				Q_QUE.remove();
			}
			int num = scanner.nextInt();
			while (!P_QUE.isEmpty() && P_QUE.getLast() < num) {
				P_QUE.removeLast();
			}
			while (!Q_QUE.isEmpty() && Q_QUE.getLast() > num) {
				Q_QUE.removeLast();
			}
			P_QUE.add(num);
			Q_QUE.add(num);
			data.add(num);
			i += 1;
			P_sum += P_QUE.getFirst();
			Q_sum += Q_QUE.getFirst();
		}
		System.out.printf("%.2f%n", (P_sum - Q_sum) / (1 + n - k));
		scanner.close();
	}
}
